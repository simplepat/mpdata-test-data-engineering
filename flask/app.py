import os
from flask import Flask, redirect, render_template, request, session, url_for, jsonify
from flask_dropzone import Dropzone
from flask_uploads import UploadSet, configure_uploads, IMAGES, patch_request_class


app = Flask(__name__)
dropzone = Dropzone(app)


app.config['SECRET_KEY'] = 'mysupersecretkeywhichiputongithub'

# Dropzone settings
app.config['DROPZONE_UPLOAD_MULTIPLE'] = True
app.config['DROPZONE_ALLOWED_FILE_CUSTOM'] = True
app.config['DROPZONE_ALLOWED_FILE_TYPE'] = 'image/*'
app.config['DROPZONE_REDIRECT_VIEW'] = 'predict'

# Upload settings
app.config['UPLOADED_PICTURES_DEST'] = os.getcwd() + '/uploads'
pictures = UploadSet('pictures', IMAGES)
configure_uploads(app, pictures)
patch_request_class(app)


"""
API Routes definition
"""

@app.route('/', methods=['GET', 'POST'])
def index():

    # set session for image results
    if "file_url" not in session:
        session['file_url'] = []

    # image upload from dropzone
    if request.method == 'POST':
        file_obj = request.files
        for f in file_obj:
            file = request.files.get(f)

            # save the file to our pictures folder
            filename = pictures.save(file, name=file.filename)

        session['file_url'] = pictures.url(filename)
        return "Uploading..."

    # if GET request, simply return
    return render_template('index.html')



@app.route('/predict', methods=['GET'])
def predict():

    # redirect to home if no image to display
    if "file_url" not in session or session['file_url'] == []:
        return redirect(url_for('index'))

    # set the file_url and remove the session variable
    file_url = session['file_url']
    session.pop('file_url', None)

    # use our model to predict a dog breed
    """
    ****** A COMPLETER ICI ******
    """
    predicted_breed = "<insert model output here>"

    return render_template('predict.html',
                            file_url=file_url,
                            predicted_breed=predicted_breed)


@app.route('/sendFeedback', methods=['POST'])
def send_feedback():

    # this is a template of the data that needs to be retrieved
    mockup_data = {
        "image_url": "/url/to/uploaded/image",
        "classification": "my_dog_breed",
        "user_feedback": 1
    }

    return jsonify(mockup_data)
