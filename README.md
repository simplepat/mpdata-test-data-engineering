## MP DATA - Test Data Engineering

## Pré-requis

- Cloner ce répository

- Créer un environnement virtuel Python 3.7 et/ou installer les packages présents dans le fichier requirements.txt:


```
	python3 -m venv my_env
	source env/bin/activate (sur linux)
	.\env\Scripts\activate  (sur windows)
	pip install -r requirements.txt
```

- Télécharger le [jeu de données d'entrainement](https://mpdata-test-data-engineering.s3.eu-west-3.amazonaws.com/dogImages.zip) puis dézipper de manière à avoir un dossier `/dogImages` à la racine du dossier de travail.

- Télécharger le [modèle ResNet50 et ses poids pré-entraînés](https://mpdata-test-data-engineering.s3.eu-west-3.amazonaws.com/DogResnet50Data.npz). Copier ce fichier dans un dossier `/bottleneck_features` à la racine du projet.


## Enoncé

Un data scientist d'une équipe client vous fournit un modèle permettant de reconnaitre une race de chien au sein d'une image (de chien ou non !) établi en tant qu'avant-projet dans un jupyter notebook (fichier `mpdata_DE_model.ipynb` à la racine du projet)

Notre client souhaite, sur la base de ce modèle, établir une application web robuste capable d'afficher à l'utilisateur la race d'un chien à partir d'une image uploadée.

Pour cela, une équipe projet nous a fourni une API REST minimaliste développée en python avec le framework [Flask](https://www.palletsprojects.com/p/flask/), et dont vous trouverez le squelette dans le dossier `/flask` du projet. 
Cette API comprend 3 routes:

- POST `/`: cette route permet d'uploader une image au travers de la dropzone sur la page

- GET `/predict`: cette route doit appeler notre modèle et retourner une classification

- POST `/sendFeedback`: cette route doit envoyer à notre serveur le retour utilisateur sur notre classification

### 1ère partie

Il vous faudra tout d'abord plugger le modèle issu du notebook à l'application Flask (i.e. compléter la route `/predict` du fichier `/flask/app.py`). L'upload d'une image doit pouvoir rediriger vers la page affichant la prédiction.


### 2ème partie

Le but sera ensuite de pipeliner et de déployer cette application, en prenant en considération les remarques suivantes:

- L'application ne doit pouvoir être déployée qu'après une série de tests "successful". Ces tests sont à compléter dans le fichier `tests_api.py` du dossier `/tests`.

- L'application doit être scalable et pouvoir ingérer jusqu'à 1000 images à la minute au travers de notre interface web

- Après upload sur le serveur, les images uploadées par l'utilisateur doivent être stockées de manière sécurisée et répliquée (Hadoop, S3...)

- Le modèle doit pouvoir être versionné et déployé indépendemment du reste de l'application (branche ou répo spécifique)

- Les dépendances lourdes, s'il y en a, (datasets ou ressources d'entraînement/test) doivent être externalisées au moment du déploiement

- L'application est critique et ne peut pas se permettre de downtime supérieur à 5 minutes

- L'application doit produire des logs permettant de retracer les évènements serveur sur une fenêtre de 6 mois, et/ou d'être ingérées dans une solution d'analyse de logs de type LogStash.


Les outils/frameworks/plateformes employés sont à la discrétion du candidat (local/AWS, Docker/Kubernetes, Kafka/Spark Streaming, ...). Il est toutefois impératif d'utiliser Jenkins pour un déploiement "on-premise" et CodePipeline pour un déploiement sur AWS.

### 3ème partie

Enfin, notre client souhaite pouvoir récupérer les images soumises par nos utilisateurs ainsi que leur retour sur la classification que nous avons émise. Avons-nous bien détecté la bonne race de chien ? L'utilisateur peut nous le dire au travers de deux boutons Vrai/Faux sur sa page de résultat.

Les données ainsi renvoyées seront de la forme: `{<image_url>, <classification>, <user_feedback>}`.

Proposer un schéma d'architecture, avec les outils/frameworks de votre choix, permettant de récupérer ces données et de réentrainer notre modèle en quasi temps-réel.


## Bon courage !


## Notes:

Installer Jenkins:

- via DockerHub: `docker pull jenkins/jenkins`

- sur Windows: https://www.blazemeter.com/blog/how-to-install-jenkins-on-windows
